﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rectangle_Assg1
{
    public class Rectangle
    {
        //private fields of Rectangle
        private int length;
        private int width;

        //default constructor builds a 1 x 1 x 1 x 1 rectangle
        public Rectangle()
        {
            this.length = 1;
            this.width = 1;
        }

        //constructor that builds a rectangle using inputs from user
        public Rectangle(int length, int width)
        {
            this.length = length;
            this.width = width;
        }

        //method to return the length of the rectangle
        public int GetLength()
        {
            return length;
        }

        //method to set the length of the rectangle
        public void SetLength(int length)
        {
            this.length = length;
        }

        //method to return the width of the rectangle
        public int GetWidth()
        {
            return width;
        }

        //method to set the length of the rectangle
        public void SetWidth(int width)
        {
            this.width = width;
        }

        //method to return the perimeter of the rectangle
        public int GetPerimeter()
        {
            int perimeter = (2 * length) + (2 * width);
            return perimeter;
        }

        //method to return the area of the rectangle
        public int GetArea()
        {
            int area = width * length;
            return area;
        }

    }
}
