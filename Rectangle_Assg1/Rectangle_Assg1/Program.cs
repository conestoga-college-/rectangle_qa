﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rectangle_Assg1
{
    class Program
    {
        private Rectangle rectangle = null;
        private int length = 0;
        private int width = 0;

        //program starts here
        static void Main(string[] args)
        {
            Program p = new Program();
            p.BuildRectangle();
            p.Menu();
        }

        //asks to the user lenght and width to build the rectangle
        public void BuildRectangle()
        {
            
            Console.WriteLine("Please enter the length and width of the rectangle.");

            GetRectangleLength();

            GetRectangleWidth();

            rectangle = new Rectangle(length, width);

            Console.WriteLine("");
        }

        //presents the menu and expects the input from the user
        public void Menu()
        {
            string sOption = string.Empty;
            int option = 0;

            do
            {
                Console.WriteLine("1. Get Rectangle Length");
                Console.WriteLine("2. Change Rectangle Length");
                Console.WriteLine("3. Get Rectangle Width");
                Console.WriteLine("4. Change Rectangle Width");
                Console.WriteLine("5. Get Rectangle Perimeter");
                Console.WriteLine("6. Get Rectangle Area");
                Console.WriteLine("7. Exit");
                Console.WriteLine("");
                sOption = Console.ReadLine();
            } while (!int.TryParse(sOption, out option) || option < 1 || option > 7);

            //depends on the input, an operation is made
            switch (option)
            {
                case 1:
                    Console.WriteLine("Rectangle Lenght: " + rectangle.GetLength().ToString());
                    Console.WriteLine("");
                    break;
                case 2:
                    GetRectangleLength();
                    rectangle.SetLength(length);
                    Console.WriteLine("");
                    break;
                case 3:
                    Console.WriteLine("Rectangle Width: " + rectangle.GetWidth().ToString());
                    Console.WriteLine("");
                    break;
                case 4:
                    GetRectangleWidth();
                    rectangle.SetWidth(width);
                    Console.WriteLine("");
                    break;
                case 5:
                    Console.WriteLine("Rectangle Perimeter: " + rectangle.GetPerimeter().ToString());
                    Console.WriteLine("");
                    break;
                case 6:
                    Console.WriteLine("Rectangle Area: " + rectangle.GetArea().ToString());
                    Console.WriteLine("");
                    break;
                case 7:
                    Environment.Exit(0);
                    break;
            }

            Menu();
        }

        //default method to get the input (length) from user
        //checks for correct input (integer and greater then 0)
        public void GetRectangleLength()
        {
            string sLength = string.Empty;

            do
            {
                Console.Write("Length: ");
                sLength = Console.ReadLine();
            } while (!int.TryParse(sLength, out length) || length <= 0);
            
        }

        //default method to get the input (width) from user
        //checks for correct input (integer and greater then 0)
        public void GetRectangleWidth()
        {
            string sWidth = string.Empty;

            do
            {
                Console.Write("Width: ");
                sWidth = Console.ReadLine();
            } while (!int.TryParse(sWidth, out width) || width <= 0);
        }
    }
}
