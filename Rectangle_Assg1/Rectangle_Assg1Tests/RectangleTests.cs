﻿using NUnit.Framework;
using Rectangle_Assg1;

namespace Rectangle_Assg1.Tests
{
    [TestFixture]
    public class RectangleTests
    {
        //global variables
        Rectangle rectangle;
        int length = 5;
        int width = 5;

        // 1. Arrange
        //Setting up the rectangle to length = 5 and width = 5 before each test
        [SetUp]
        public void SetUp()
        {
            rectangle = new Rectangle(length, width);
        }

        // Test Identifier: 1.1
        // Test Description: Success: Returning the length = 5 of a rectangle
        // Input Data: 5
        // Expected Output: 5
        [Test]
        public void GetLengthTest_input5_outuput5()
        {
            // 2. Act
            int response = rectangle.GetLength();

            // 3. Assert
            Assert.AreEqual(length, response);
        }

        // Test Identifier: 1.2
        // Test Description: Success: Setting a new length = 6 of the rectangle
        // Input Data: 6
        // Expected Output: 6
        [Test]
        public void SetLengthTest_newInputLength6_output6()
        {
            // 1. Arrange
            int newLength = 6;

            // 2. Act
            rectangle.SetLength(newLength);
            //getting the new length
            int response = rectangle.GetLength();

            // 3. Assert
            Assert.AreEqual(newLength, response);
        }

        // Test Identifier: 1.3
        // Test Description: Success: Returning the width = 5 of a rectangle
        // Input Data: 5
        // Expected Output: 5
        [Test]
        public void GetWidthTest_input5_outuput5()
        {
            // 2. Act
            int response = rectangle.GetWidth();

            // 3. Assert
            Assert.AreEqual(width, response);
        }

        // Test Identifier: 1.4
        // Test Description: Success: Setting a new width = 7 of the rectangle
        // Input Data: 7
        // Expected Output: 7
        [Test]
        public void SetWidthTest_newInputLength7_output7()
        {
            // 1. Arrange
            int newWidth = 7;

            // 2. Act
            rectangle.SetWidth(newWidth);
            //getting the new length
            int response = rectangle.GetWidth();

            // 3. Assert
            Assert.AreEqual(newWidth, response);
        }

        // Test Identifier: 1.5
        // Test Description: Success: Getting the perimeter of the rectangle
        // Input Data: 5,5,5,5
        // Expected Output: 20
        [Test]
        public void GetPerimeterTest_input5555_output20()
        {
            // 2. Act
            //accessing the method to get the perimeter
            int response = rectangle.GetPerimeter();

            // 3. Assert
            Assert.AreEqual(20, response);
        }

        // Test Identifier: 1.6
        // Test Description: Success: Getting the area of the rectangle
        // Input Data: 5,5
        // Expected Output: 25
        [Test]
        public void GetAreaTest_input55_output25()
        {
            // 2. Act
            //accessing the method to get the area
            int response = rectangle.GetArea();

            // 3. Assert
            Assert.AreEqual(25, response);
        }

    }
}